﻿using DAL;
using Shopping.BusinessObject.models;
using System.Data;

namespace BAL
{
    public class Bal
    {
        Dal dal = new Dal();

        public int AddUser(User user)
        {
            dal.AddUser(user);
            return 0;
        }
        public int DeleteUser(int id)
        {
            dal.DeleteUser(id);
            return 1;
        }
        public Role GetRole(int roleid)
        {
            return dal.GetRole(roleid);

        }
        

        //public int EditUser(int id, User user)
        //{
        //    dal.EditUser(id, user);
        //    return 0;
        //}
        public int AddProduct(Product product)
        {
            dal.AddProduct(product);
            return 1;
        }
        public int DeleteProduct(int id)
        {
            dal.DeleteProduct(id);
            return 1;
        }
        //public int GetProduct(string name)
        //{
        //    dal.GetProduct(name);
        //    return 0;
        //}
        public int UpdateProduct(string name, Product product)
        {
            dal.UpdateProduct(name, product);



            return 1;



        }
        //public int DisplayProduct(Product product)
        //{
        //    dal.DisplayProduct();
        //    return 0;
        //}

        public List<Product> GetProducts()
        {
            return dal.GetProducts();
        }



        public int PlaceOrder(Order order)
        {
            return dal.PlaceOrder(order);
        }



        public List<User> GetUsers()
        {
            return dal.GetUsers();
        }



        //public int PlaceOrder(Order order)
        //{
        //    return dal.PlaceOrder(order);
        //}



    }
}