﻿
//using Shopping.BusinessObject.models;
using BAL;
using DAL;
using Shopping.BusinessObject.models;

namespace Shopping
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Bal bal = new Bal();
            //Console.WriteLine("Please enter the details");
            //Console.WriteLine("Enter UserName");
            //string name = Console.ReadLine();

            //Console.WriteLine("You are customer or supplier");
            //Console.WriteLine("Enter 2 for SUPPLIER , 3 for CUSTOMER");

            //int role = Byte.Parse(Console.ReadLine());
            //Role userRole = bal.GetRole(role);
            //User user = new User()
            //{
            //    UserName = name,
            //    Role = userRole
            //};
            //bal.AddUser(user);
            //Console.WriteLine("Your product is added");
            //Console.WriteLine("Want to edit your product");
            //Bal bal1 = new Bal();
            //Console.WriteLine("Enter id");
            //int id = Byte.Parse(Console.ReadLine());
            //Console.WriteLine("Enter UserName");
            //name = Console.ReadLine();

            //Console.WriteLine("Enter 2 for supplier , 3 for customer");
            //role = Byte.Parse(Console.ReadLine());
            //userRole = bal.GetRole(role);
            //user = new User()
            //{
            //    UserName = name,

            //    Role = userRole
            //};
            //bal1.EditUser(id, user);



            Console.WriteLine("WELCOME TO ONLINE SHOPPING SITE");
            Console.WriteLine("Choose whatever you want");
            Console.WriteLine("What is your role? \n1.Admin\n2.Supplier\n3.Customer\n");
            int ch = Convert.ToInt32(Console.ReadLine());
            //Role role = bal.GetUserRole(Convert.ToString(ch));
            switch (ch)
            {
                case 1:
                    {
                        Console.WriteLine("1.Add User\n2.Delete User");
                        //int choice = Convert.ToInt32(Console.ReadLine());
                        int choice = Byte.Parse(Console.ReadLine());
                        if (choice == 1)
                        {
                            Console.WriteLine("Enter UserName");
                            string name = Console.ReadLine();

                            // Role userRole = bal.GetRole();
                            User user = new User()
                            {
                                UserName = name,
                                IsActive = true
                                //Role = userRole
                            };
                            bal.AddUser(user);
                            Console.WriteLine("Your product is added");

                        }
                        else if (choice == 2)
                        {
                            
                            Console.WriteLine("enter user id");
                            int id = Convert.ToInt32(Console.ReadLine());
                            bal.DeleteUser(id);

                        }

                        break;


                    }
                case 2:
                    {
                        Console.WriteLine("1.Add Product\n2.Delete Product\n3.Update Product\n");
                        int choice = Convert.ToInt32(Console.ReadLine());
                        if (choice == 1)
                        {
                            Console.WriteLine("Enter Product Name");
                            string pName = Console.ReadLine();
                            Console.WriteLine("Enter Product price");
                            int pPrice = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter Quantity:");
                            int pQty = Convert.ToByte(Console.ReadLine());
                            Console.WriteLine("Enter Product Description");
                            string pDes = Console.ReadLine();

                            //Console.WriteLine("Which Role");
                            Product product = new Product()
                            {
                                ProductName = pName,
                                Price = pPrice,
                                Quantity = pQty,
                                Description = pDes
                            };
                            int res = bal.AddProduct(product);
                            Console.WriteLine(res);
                        }
                        else if (choice == 2)
                        {
                            Console.WriteLine("Enter user id");
                            int Id = Convert.ToInt32(Console.ReadLine());
                            bal.DeleteProduct(Id);



                        }
                        else if (choice == 3)
                        {
                            Console.WriteLine("Enter the name of the product to edit");
                            string name = Console.ReadLine();
                            ////Product product = bal.GetProduct(name);
                            //if (product != null)
                            //{
                            Console.WriteLine("Enter the new name");
                            string? ProductName = Console.ReadLine();
                            Console.WriteLine("Enter the new price");
                            //int Price = Convert.ToInt32(Console.ReadLine());
                            int Price;
                            Int32.TryParse(Console.ReadLine(), out Price);
                            Console.WriteLine("Enter the new Quantity");
                            int Quantity;
                            Int32.TryParse(Console.ReadLine(), out Quantity);
                            Console.WriteLine("Enter Product Description");
                            string pDes = Console.ReadLine();
                            Product upDatedProduct = new Product()
                            {
                                ProductName = ProductName,
                                Price = Price,
                                Quantity = Quantity,
                                Description = pDes
                            };

                            bal.UpdateProduct(name, upDatedProduct);
                            //}




                        }
                        break;
                    }
                case 3:
                    {
                        //Console.WriteLine("You have selected Customer");
                        //Console.WriteLine("What do you want? \n 1. See all product \n 2.Place order");
                        //int type = Convert.ToInt32(Console.ReadLine());
                        //if (opt == 1)
                        //{
                        //    Console.WriteLine("All Products::::");
                        //    DisplayProduct();




                        //}
                        //else if (opt == 2)
                        //{

                        //




                        Console.WriteLine("1.Do you want to display all the products");

                        Console.WriteLine("2.Do you want to place order");

                        int type = Convert.ToInt32(Console.ReadLine());

                        if (type == 1)

                        {
                            List<Product> list = bal.GetProducts();

                            foreach (Product prod in list)

                            {

                                Console.WriteLine(prod.ProductName + " " + prod.Quantity + " " + prod.Price + " " + prod.Description);

                            }

                        }

                        else

                        {

                            Order order = new Order()

                            {

                                CreatedAt = DateTime.Now

                            };

                            bal.PlaceOrder(order);

                        }


                    }
                    break;
                    default:
                    Console.WriteLine("Invalid Choice");
                    break;

            }
            Console.WriteLine("Do you want to repeat?");
            ch = Convert.ToChar(Console.ReadLine());

        }
        //public static void DisplayProduct()
        //{
        //    Bal bal = new Bal();
        //    Product product = new Product();
        //    bal.DisplayProduct(product);

        //}


    }
    }

    
