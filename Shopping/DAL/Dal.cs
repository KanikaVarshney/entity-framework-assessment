﻿using Shopping.BusinessObject.models;
using Shopping.DAL.Context;
using System.Data;

namespace DAL
{
    public class Dal
    {
        ShoppingDbContext db = new ShoppingDbContext();
        
        //public int AddUser(User user)
        //{
        //    db.Users.Add(user);
        //    SaveRecords();
        //    return 0;

        //}
        public int AddUser(User user)
        {
            List<Role> Roles = db.Roles.ToList();
            Console.WriteLine(Roles);
            Console.WriteLine("Choose role id:\n 2 for Supplier\n 3 for Customer");
            int RoleId = Convert.ToInt32(Console.ReadLine());



            Role obj = db.Roles.Find(RoleId);
            user.Role = obj;
            user.Role.RoleID = RoleId;
            db.Users.Add(user);
            db.SaveChanges();
            Console.WriteLine("User has been  added!");
            return 0;
        }
        public Role GetRole(int id)
        {
            Role role = db.Roles.Find(id);
            return role;
        }
        public int DeleteUser(int id)
        {
            User obj = db.Users.Where(x => x.UserID == id).FirstOrDefault();
            if (obj != null)
            {
                db.Users.Remove(obj);
                Console.WriteLine("User deleted!");
                db.SaveChanges();
                return 0;
            }
            else
            {
                Console.WriteLine("No such user exists with this id");
                return 1;
            }
        }


        void SaveRecords()
        {
            db.SaveChanges();
        }

        public int EditUser(int id, User user)
        {



            User obj = db.Users.FirstOrDefault(x => x.UserID == id);
            if (obj != null)
            {
                for (int i = 0; i < db.Users.ToList().Count; i++)
                {
                    if (obj.UserID == id)
                    {
                        obj.UserName = user.UserName;
                        




                    }
                }
                db.SaveChanges();
            }


            return 0;
        }
        


        public int AddProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
            return 1;
        }

        public int DeleteProduct(int id)
        {
            Product obj = db.Products.Where(x => x.ProductID == id).FirstOrDefault();
            if (obj != null)
            {
                db.Products.Remove(obj);
                db.SaveChanges();
                Console.WriteLine("Product has been deleted");
                return 0;
            }
            else
            {
                Console.WriteLine("No such product exists");
                return 1;
            }
        }



        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.ProductName == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.ProductName.Length > 0)
                    obj.ProductName = product.ProductName;
                if (product.Price > 0)
                    obj.Price = product.Price;

                if (product.Quantity > 0)
                    obj.Quantity = product.Quantity;

                db.Products.Update(obj);
                db.SaveChanges();
            }
            else
            {
                return 1;
            }

            return 0;

        }
        


        //public Product GetProduct(string name)
        //{
        //    //Product product = db.Products.Find(name);
        //    Product product = db.Products.Where(x => x.ProductName == name).FirstOrDefault();
        //    return product;

        //}
        public List<Product> DisplayProduct()
        {

            List<Product> list = new List<Product>();
            list = db.Products.ToList();
            foreach (Product temp in list)
            {
                Console.WriteLine($"{temp.ProductName}--{temp.Quantity}");
            }

            return list;
        }


        //public int AddOrder(Order order)
        //{
        //    db.Orders.Add(order);
        //    db.SaveChanges();   
        //    return 0;
        //}

        //public List<Product> GetProducts()
        //{
        //    List<Product> list = db.Products.ToList();



        //    return list;
        //}




        //public Role GetRole()
        //{
        //    return (db.Roles.FirstOrDefault(x => x.Name == "Customer"));
        //}

        public List<Product> GetProducts()
        {
            List<Product> list = db.Products.ToList();

            return list;
        }

        public int PlaceOrder(Order order)
        {
            List<Product> products = db.Products.ToList();
            foreach (Product product in products)
            {
                Console.WriteLine(product.ProductID + " " + product.ProductName + " " + product.Price + " " +
                product.Quantity + " " + product.Description);
            }
            Console.WriteLine("select product id");
            int prodId = Convert.ToInt32(Console.ReadLine());
            List<User> users = db.Users.Where(x => x.Role.RoleID == 2).ToList();
            foreach (User usr in users)
            {
                Console.WriteLine(usr.UserID + " " + usr.UserName);
            }
            Console.WriteLine("select user id");
            int userId = Convert.ToInt32(Console.ReadLine());



            Product ProductObj = db.Products.Find(prodId);
            User UserObj = db.Users.Find(userId);



            order.Product = ProductObj;
            order.User = UserObj;



            db.Orders.Add(order);
            db.SaveChanges();
            Console.WriteLine(" GREAT!!!!Your order has been placed!");
            return 0;



        }
        public List<User> GetUsers()
        {
            List<User> list = db.Users.ToList();
            return list;
        }


    }
}